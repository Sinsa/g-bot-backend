package ch.discordbot.gbot;

import ch.discordbot.gbot.bot.Bot;
import ch.discordbot.gbot.bot.Command;
import ch.discordbot.gbot.bot.utils.Usergroup;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.message.MessageAuthor;
import org.javacord.api.entity.permission.Permissions;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author Sinsa#2674
 * @since 0.1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class BotTest {

    private final Bot bot = new Bot();
    private final Bot spyBot = spy(bot);
    private final MessageCreateEvent mockMessageCreateEvent = mock(MessageCreateEvent.class);
    private final Server mockServer = mock(Server.class);
    private final User mockUser = mock(User.class);
    private final MessageAuthor mockMessageAuthor = mock(MessageAuthor.class);
    private final Permissions mockPermissions = mock(Permissions.class);
    private final Command mockCommand = mock(Command.class);
    private final ServerTextChannel mockServerTextChannel = mock(ServerTextChannel.class);

    @Before
    public void setupTests() {
        when(mockMessageCreateEvent.isServerMessage()).thenReturn(true);
        when(mockMessageCreateEvent.getMessageAuthor()).thenReturn(mockMessageAuthor);
        when(mockMessageAuthor.asUser()).thenReturn(java.util.Optional.ofNullable(mockUser));
        when(mockMessageCreateEvent.getServer()).thenReturn(java.util.Optional.ofNullable(mockServer));
        assertNotNull(mockServer);
        when(mockServer.getPermissions(mockUser)).thenReturn(mockPermissions);
    }


    @Test
    public void testResolveCommandAuthorPermissionLevel() {
        when(mockPermissions.getAllowedBitmask()).thenReturn(35848); // Admin, ReadMessages, SendMessages, Attach Files
        assertEquals(Usergroup.ADMINISTRATOR, bot.resolveCommandAuthorPermissionLevel(mockMessageCreateEvent));
        when(mockPermissions.getAllowedBitmask()).thenReturn(46); // Admin, Manage Server, Kick, Ban
        assertEquals(Usergroup.ADMINISTRATOR, bot.resolveCommandAuthorPermissionLevel(mockMessageCreateEvent));
        when(mockPermissions.getAllowedBitmask()).thenReturn(38); // Manage Server, Kick, Ban
        assertEquals(Usergroup.MODERATOR, bot.resolveCommandAuthorPermissionLevel(mockMessageCreateEvent));
        when(mockPermissions.getAllowedBitmask()).thenReturn(14470); // Auditlog, Kick, Ban, SendMessages, SendTTSMessages, Manage Messages
        assertEquals(Usergroup.MODERATOR, bot.resolveCommandAuthorPermissionLevel(mockMessageCreateEvent));
        when(mockPermissions.getAllowedBitmask()).thenReturn(1275595905); // Auditlog, Server Insights, CreateInvite, Change/Manage Nickname, Manage Emojis, Read/Send Messages
        assertEquals(Usergroup.USER, bot.resolveCommandAuthorPermissionLevel(mockMessageCreateEvent));
        when(mockPermissions.getAllowedBitmask()).thenReturn(3072); // Read/Send Messages
        assertEquals(Usergroup.USER, bot.resolveCommandAuthorPermissionLevel(mockMessageCreateEvent));
        when(mockMessageCreateEvent.isServerMessage()).thenReturn(false);
        assertEquals(Usergroup.USER, bot.resolveCommandAuthorPermissionLevel(mockMessageCreateEvent));
    }

    @Test
    public void testHasUserPermission() {
        when(mockCommand.getMinAccess()).thenReturn(Usergroup.USER);
        doReturn(Usergroup.USER).when(spyBot).resolveCommandAuthorPermissionLevel(any());
        assertTrue(spyBot.hasUserPermission(mockCommand, mockMessageCreateEvent));
        when(mockCommand.getMinAccess()).thenReturn(Usergroup.MODERATOR);
        doReturn(Usergroup.USER).when(spyBot).resolveCommandAuthorPermissionLevel(any());
        assertFalse(spyBot.hasUserPermission(mockCommand, mockMessageCreateEvent));
        when(mockCommand.getMinAccess()).thenReturn(Usergroup.ADMINISTRATOR);
        doReturn(Usergroup.USER).when(spyBot).resolveCommandAuthorPermissionLevel(any());
        assertFalse(spyBot.hasUserPermission(mockCommand, mockMessageCreateEvent));

        when(mockCommand.getMinAccess()).thenReturn(Usergroup.USER);
        doReturn(Usergroup.MODERATOR).when(spyBot).resolveCommandAuthorPermissionLevel(any());
        assertTrue(spyBot.hasUserPermission(mockCommand, mockMessageCreateEvent));
        when(mockCommand.getMinAccess()).thenReturn(Usergroup.MODERATOR);
        doReturn(Usergroup.MODERATOR).when(spyBot).resolveCommandAuthorPermissionLevel(any());
        assertTrue(spyBot.hasUserPermission(mockCommand, mockMessageCreateEvent));
        when(mockCommand.getMinAccess()).thenReturn(Usergroup.ADMINISTRATOR);
        doReturn(Usergroup.MODERATOR).when(spyBot).resolveCommandAuthorPermissionLevel(any());
        assertFalse(spyBot.hasUserPermission(mockCommand, mockMessageCreateEvent));

        when(mockCommand.getMinAccess()).thenReturn(Usergroup.USER);
        doReturn(Usergroup.ADMINISTRATOR).when(spyBot).resolveCommandAuthorPermissionLevel(any());
        assertTrue(spyBot.hasUserPermission(mockCommand, mockMessageCreateEvent));
        when(mockCommand.getMinAccess()).thenReturn(Usergroup.MODERATOR);
        doReturn(Usergroup.ADMINISTRATOR).when(spyBot).resolveCommandAuthorPermissionLevel(any());
        assertTrue(spyBot.hasUserPermission(mockCommand, mockMessageCreateEvent));
        when(mockCommand.getMinAccess()).thenReturn(Usergroup.ADMINISTRATOR);
        doReturn(Usergroup.ADMINISTRATOR).when(spyBot).resolveCommandAuthorPermissionLevel(any());
        assertTrue(spyBot.hasUserPermission(mockCommand, mockMessageCreateEvent));
    }

    @Test
    public void testResolveUsergroupEnumValue() {
        assertEquals(0, bot.resolveUsergroupEnumValue(Usergroup.USER));
        assertEquals(1, bot.resolveUsergroupEnumValue(Usergroup.MODERATOR));
        assertEquals(2, bot.resolveUsergroupEnumValue(Usergroup.ADMINISTRATOR));
    }

    @Test
    public void testIsOnServerWithServerOnly() {
        when(mockCommand.isServerOnly()).thenReturn(false);
        assertFalse(bot.isOnServerWithServerOnly(mockMessageCreateEvent, mockCommand));

        when(mockCommand.isServerOnly()).thenReturn(true);
        when(mockMessageCreateEvent.getServerTextChannel()).thenReturn(Optional.ofNullable(mockServerTextChannel));
        assertTrue(bot.isOnServerWithServerOnly(mockMessageCreateEvent, mockCommand));

        when(mockMessageCreateEvent.getServerTextChannel()).thenReturn(Optional.empty());
        assertFalse(bot.isOnServerWithServerOnly(mockMessageCreateEvent, mockCommand));
    }

}
