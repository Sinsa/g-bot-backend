package ch.discordbot.gbot.logging;

public enum Action {
    KICK,
    TEMP_BAN,
    BAN,
    WARN,
    CLEAR
}
