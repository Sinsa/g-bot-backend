package ch.discordbot.gbot.logging;

import org.javacord.api.DiscordApi;
import org.javacord.api.entity.channel.Channel;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.Collection;
import java.util.List;

public class ActionLogger {
    private TextChannel channel;

    public void log(MessageCreateEvent event, Action action, String[] params) {
        this.channel = findLogChannel(event);
        switch (action) {
            case KICK:
                channel.sendMessage(buildKickEmbed(event, params));
                break;
            case BAN:
                channel.sendMessage(buildBanEmbed(event, params));
                break;
            case TEMP_BAN:
                channel.sendMessage(buildTempBanEmbed(event, params));
                break;
            case WARN:
                channel.sendMessage(buildWarnEmbed(event, params));
                break;
            case CLEAR:
                channel.sendMessage(buildClearEmbed(event, params));
                break;
        }
    }

    private EmbedBuilder buildKickEmbed(MessageCreateEvent event, String[] params) {
        String kickedUser = params[0];
        return new EmbedBuilder()
                .setAuthor(event.getMessageAuthor())
                .setDescription(kickedUser + " has been kicked by " + event.getMessageAuthor().getDiscriminatedName());
    }

    private EmbedBuilder buildBanEmbed(MessageCreateEvent event, String[] params) {
        String bannedUser = params[0];
        String reason = params[1];
        StringBuilder stringBuilder = new StringBuilder();
        return new EmbedBuilder()
                .setAuthor(event.getMessageAuthor())
                .setDescription(stringBuilder.append(bannedUser)
                        .append(" was banned by ")
                        .append(event.getMessageAuthor().getDiscriminatedName())
                        .append("\nReason: ")
                        .append(reason).toString());
    }

    private EmbedBuilder buildTempBanEmbed(MessageCreateEvent event, String[] params) {
        String bannedUser = params[0];
        return new EmbedBuilder()
                .setAuthor(event.getMessageAuthor())
                .setDescription(bannedUser + " banned temporarily by " + event.getMessageAuthor().getDiscriminatedName())
                .addField("Duration", "-"); // TODO: Duration
    }

    private EmbedBuilder buildWarnEmbed(MessageCreateEvent event, String[] params) {
        String warnedUser = params[0];
        return new EmbedBuilder()
                .setAuthor(event.getMessageAuthor())
                .setDescription(warnedUser + " was warned by " + event.getMessageAuthor().getDiscriminatedName());
        //.addField("Total Warnings", "-"); // TODO: Total Warnings
    }

    private EmbedBuilder buildClearEmbed(MessageCreateEvent event, String[] params) {
        String deletedMessages = "all";
        if (params.length > 0) {
            deletedMessages = params[0];
        }
        return new EmbedBuilder()
                .setAuthor(event.getMessage().getAuthor())
                .setDescription(event.getChannel() + " has been cleared by " + event.getMessageAuthor().getDiscriminatedName())
                .addField("Deleted Messages", deletedMessages);
    }

    private TextChannel findLogChannel(MessageCreateEvent event) {
        Channel channel = null;
        List<ServerTextChannel> textChannels = event.getServer().get().getTextChannels();
        for (ServerTextChannel textChannel : textChannels) {
            if (textChannel.getName().contains("log")) {
                channel = textChannel;
            }
        }
        return channel.asTextChannel().get();
    }
}
