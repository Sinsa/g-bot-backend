package ch.discordbot.gbot.drivers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.Arrays;

public class Mariadb {
    private static final String JDBC_DRIVER = "org.mariadb.jdbc.Driver";
    private static final String SSH_URL = "db.sinsa92.net";
    private static final int DB_PORT = 3306;
    private static final String DB_USER = "gbot";
    private static final String DB_PASSWORD = "45hsf93ks1cx"; // TODO Fix before publishing (after Module 306)
    private static final String DB_DATABASE = "gbot";
    private static final String DB_URL = "jdbc:mariadb://" + SSH_URL + ":" + DB_PORT + "/";
    static Logger logger = LoggerFactory.getLogger(Mariadb.class);

    /**
     * This Method is for inserting values in the MariaDB.
     *
     * @param tableName    The table you'd like to insert the values into
     * @param settingRow   Array of rows that should be affected
     * @param settingValue Array of values that should be inserted (Need to be at the same array position as the according row in the settingRow array
     */
    public void insert(String tableName, String[] settingRow, String[] settingValue) {
        Connection connection;
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL + DB_DATABASE, DB_USER, DB_PASSWORD);
            StringBuilder rowsStringForQuery = new StringBuilder();
            StringBuilder valuesStringForQuery = new StringBuilder();
            for (String singleRow : settingRow) {
                rowsStringForQuery.append(singleRow).append(", ");
            }
            for (String singleValue : settingValue) {
                valuesStringForQuery.append("'").append(singleValue).append("', ");
            }
            rowsStringForQuery.deleteCharAt(rowsStringForQuery.length() - 1);
            rowsStringForQuery.deleteCharAt(rowsStringForQuery.length() - 1);
            valuesStringForQuery.deleteCharAt(valuesStringForQuery.length() - 1);
            valuesStringForQuery.deleteCharAt(valuesStringForQuery.length() - 1);
            String rawSql = "INSERT INTO " + tableName + "(" + rowsStringForQuery.toString() + ") VALUES (" + valuesStringForQuery.toString() + ");";
            try {
                Statement statement = connection.createStatement();
                int update = statement.executeUpdate(rawSql);
                if (update >= 1) {
                    logger.info("Query executed: " + rawSql);
                } else {
                    logger.warn("Query not executed:" + rawSql);
                }
            } catch (SQLException e) {
                logger.warn("SQL Statement '" + rawSql + "' was not executed!");
                e.printStackTrace();
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * This Method is for updating values in the MariaDB.
     *
     * @param tableName    The table you'd like to update
     * @param settingKey   The Row you'd like to update
     * @param settingValue The value you'd like to set
     * @param whereKey     Search criteria
     * @param whereValue   Search criteria value
     */
    public void update(String tableName, String settingKey, String settingValue, String whereKey, String whereValue) {
        Connection connection;
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL + DB_DATABASE, DB_USER, DB_PASSWORD);
            String rawSql = "UPDATE " + tableName + " SET " + settingKey + " = '" + settingValue + "' WHERE " + whereKey + "='" + whereValue + "'";
            try {
                Statement statement = connection.createStatement();
                int update = statement.executeUpdate(rawSql);
                if (update >= 1) {
                    logger.info("Row " + settingKey + " successfully updated. Query: " + rawSql);
                } else {
                    logger.info("Row " + settingKey + " not updated.");
                }
            } catch (SQLException e) {
                logger.warn("SQL Statement '" + rawSql + "' was not executed!");
                logger.warn(Arrays.toString(e.getStackTrace()));
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public ResultSet select(String table, String whereKey, String whereValue) {
        Connection connection;
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL + DB_DATABASE, DB_USER, DB_PASSWORD);
            String rawSql = "SELECT * FROM " + table + " WHERE " + whereKey + "='" + whereValue + "'";
            try {
                Statement statement = connection.createStatement();
                return (statement.executeQuery(rawSql));
            } catch (SQLException e) {
                logger.warn("SQL Statement '" + rawSql + "' was not executed!");
                logger.warn(Arrays.toString(e.getStackTrace()));
                return null;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public ResultSet select(String table) {
        Connection connection;
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL + DB_DATABASE, DB_USER, DB_PASSWORD);
            String rawSql = "SELECT * FROM " + table;
            try {
                Statement statement = connection.createStatement();
                return (statement.executeQuery(rawSql));
            } catch (SQLException e) {
                logger.warn("SQL Statement '" + rawSql + "' was not executed!");
                logger.warn(Arrays.toString(e.getStackTrace()));
                return null;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public void delete(String table, String whereKey, String whereValue) {
        try {
            Class.forName(JDBC_DRIVER);
            Connection connection = DriverManager.getConnection(DB_URL, DB_DATABASE, DB_PASSWORD);
            String rawSql = "DELETE FROM " + table + " WHERE " + whereKey + "=" + whereValue;
            try {
                Statement statement = connection.createStatement();
                int update = statement.executeUpdate(rawSql);
                if (update >= 1) {
                    logger.debug("Successfully delete from " + table + "with filter " + whereKey + "=" + whereValue);
                } else {
                    logger.warn("Could not delete from " + table + "with filter " + whereKey + "=" + whereValue);
                }
            } catch (SQLException e) {
                logger.warn("SQL Statement '" + rawSql + "' was not executed! Stacktrace below.");
                e.printStackTrace();
            }
        } catch (ClassNotFoundException | SQLException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }
}
