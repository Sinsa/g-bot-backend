package ch.discordbot.gbot.bot;

import ch.discordbot.gbot.bot.utils.Usergroup;

public class Command {
    protected Usergroup minAccess;
    protected String commandName;
    protected boolean serverOnly;

    public Command(Usergroup minAccess, boolean serverOnly, String commandName) {
        this.minAccess = minAccess;
        this.serverOnly = serverOnly;
        this.commandName = commandName;
    }

    public boolean isServerOnly() {
        return serverOnly;
    }

    public Usergroup getMinAccess() {
        return minAccess;
    }

    public String getCommandName() {
        return commandName;
    }

}
