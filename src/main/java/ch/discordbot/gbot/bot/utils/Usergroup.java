package ch.discordbot.gbot.bot.utils;

public enum Usergroup {
    ADMINISTRATOR,
    MODERATOR,
    USER
}
