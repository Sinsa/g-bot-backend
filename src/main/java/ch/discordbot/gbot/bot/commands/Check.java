package ch.discordbot.gbot.bot.commands;

import ch.discordbot.gbot.bot.Command;
import ch.discordbot.gbot.bot.utils.Usergroup;
import ch.discordbot.gbot.drivers.Mariadb;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.sql.ResultSet;

public class Check extends Command {
    Logger logger = LoggerFactory.getLogger(Check.class);
    Mariadb mariadb = new Mariadb();

    public Check(Usergroup minAccess, boolean serverOnly) {
        super(minAccess, serverOnly, "check");
    }

    public void executeCommand(MessageCreateEvent event, DiscordApi api, String[] params) {
        if (params.length > 0) {
            String userId;
            if (params[0].startsWith("<@!") && params[0].endsWith(">")) {
                userId = params[0].substring(3, params[0].length() - 1);
            } else {
                userId = params[0];
            }
            User user;
            try {
                user = api.getUserById(userId).get();
                if (user != null) {
                    ResultSet result = mariadb.select("warn", "user_id", user.getIdAsString());
                    EmbedBuilder embed = new EmbedBuilder()
                            .setTitle("Warns for " + user.getDiscriminatedName())
                            .setColor(Color.ORANGE)
                            .setTimestampToNow();
                    StringBuilder description = new StringBuilder();
                    if (!result.next()) {
                        description.append("This user was not warned yet.");
                    }
                    result.beforeFirst();
                    while (result.next()) {
                        String reason;
                        if (result.getString("reason") == null || result.getString("reason").equals("null")) {
                            reason = "No reason provided";
                        } else {
                            reason = result.getString("reason");
                        }
                        String singleResult = "```\n" + "Warn-ID :  " + result.getString("id") + "\n" +
                                "Reason  :  " + reason + "\n" +
                                "Staff   :  " + api.getUserById(result.getString("staff_id")).get().getDiscriminatedName() + " (" + result.getString("staff_id") + ")\n```\n";
                        description.append(singleResult);
                    }
                    embed.setDescription(description.toString());
                    event.getChannel().sendMessage(embed);
                }
            } catch (Exception e) {
                logger.warn("{} tried to check a nonexistend user!", event.getMessageAuthor().getDiscriminatedName());
                e.printStackTrace();
            }
        } else {
            event.getChannel().sendMessage("You need to specify a user you want to check!");
        }
    }
}
