package ch.discordbot.gbot.bot.commands;

import ch.discordbot.gbot.bot.Command;
import ch.discordbot.gbot.bot.utils.Usergroup;
import ch.discordbot.gbot.logging.Action;
import ch.discordbot.gbot.logging.ActionLogger;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Ban extends Command {
    Logger logger = LoggerFactory.getLogger(Ban.class);
    ActionLogger actionLogger = new ActionLogger();

    public Ban(Usergroup minAccess, boolean serverOnly) {
        super(minAccess, serverOnly, "ban");
    }

    public void executeCommand(MessageCreateEvent event, DiscordApi api, String[] params) throws Exception {
        if (params.length > 0) {
            User userToBeBanned = resolveIdOrMentionToUser(params[0], event);
            String reason = null;
            if (params.length > 1) {
                StringBuilder reasonBuilder = new StringBuilder();
                for (String param : params) {
                    if (!param.equals(params[0])) {
                        reasonBuilder.append(param).append(" ");
                    }
                }
                reason = reasonBuilder.substring(0, reasonBuilder.toString().length() - 1);
            }
            if (reason == null) {
                reason = "No Reason specified";
            }
            String responseReason = reason + ", ban by " + event.getMessageAuthor().getDiscriminatedName();
            Server server = event.getServer().get();
            server.banUser(userToBeBanned, 0, responseReason).get();
            event.getChannel().sendMessage(userToBeBanned.getDiscriminatedName() + " was banned successfully..");
            String[] logReason = new String[2];
            logReason[0] = userToBeBanned.getDiscriminatedName();
            logReason[1] = reason;
            actionLogger.log(event, Action.BAN, logReason);
        } else {
            logger.warn(event.getMessageAuthor().getDiscriminatedName() + " tried to use " + this.commandName + ", but passed too few arguments!");
            event.getChannel().sendMessage("You need to specify a user (via ID or mention) you'd like to ban!");
        }
    }

    private User resolveIdOrMentionToUser(String rawIdOrMention, MessageCreateEvent event) {
        Server server = event.getServer().get();
        if (rawIdOrMention.startsWith("<@!")) {
            if (server.getMemberById(rawIdOrMention.substring(3, rawIdOrMention.length() - 1)).isPresent()) {
                return server.getMemberById(rawIdOrMention.substring(3, rawIdOrMention.length() - 1)).get();
            } else {
                return null;
            }
        } else {
            if (server.getMemberById(rawIdOrMention).isPresent()) {
                return server.getMemberById(rawIdOrMention).get();
            } else {
                return null;
            }
        }
    }
}
