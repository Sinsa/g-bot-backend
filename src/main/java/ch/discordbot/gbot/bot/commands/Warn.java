package ch.discordbot.gbot.bot.commands;

import ch.discordbot.gbot.bot.Command;
import ch.discordbot.gbot.bot.utils.Usergroup;
import ch.discordbot.gbot.drivers.Mariadb;
import ch.discordbot.gbot.logging.Action;
import ch.discordbot.gbot.logging.ActionLogger;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Warn extends Command {
    static Logger logger = LoggerFactory.getLogger(Warn.class);
    private final Mariadb mariadb = new Mariadb();
    ActionLogger actionLogger = new ActionLogger();

    public Warn(Usergroup minAccess, boolean serverOnly) {
        super(minAccess, serverOnly, "warn");
    }

    public void executeCommand(MessageCreateEvent event, DiscordApi api, String[] params) {
        if (params.length > 0) {
            String userId;
            if (params[0].startsWith("<@!") && params[0].endsWith(">")) {
                userId = params[0].substring(3, params[0].length() - 1);
            } else {
                userId = params[0];
            }
            String reason = null;
            if (params.length > 1) {
                StringBuilder reasonBuilder = new StringBuilder();
                for (String param : params) {
                    if (!param.equals(params[0])) {
                        reasonBuilder.append(param).append(" ");
                    }
                }
                reason = reasonBuilder.substring(0, reasonBuilder.toString().length() - 1);
            }
            User user;
            try {
                user = api.getUserById(userId).get();
                if (user != null) {
                    String[] rows = new String[4];
                    rows[0] = "user_id";
                    rows[1] = "staff_id";
                    rows[2] = "reason";
                    rows[3] = "guild_id";
                    String[] values = new String[4];
                    values[0] = user.getIdAsString();
                    values[1] = event.getMessageAuthor().getIdAsString();
                    values[2] = reason;
                    values[3] = event.getServer().get().getIdAsString();
                    mariadb.insert("warn", rows, values);
                    if (reason != null) {
                        event.getChannel().sendMessage("I've successfully warned *" + user.getDiscriminatedName() + "* with the reason `" + reason + "`.");
                    } else {
                        event.getChannel().sendMessage("I've successfully warned " + user.getDiscriminatedName() + ".");
                    }
                    String[] logReason = new String[1];
                    logReason[0] = user.getDiscriminatedName();
                    actionLogger.log(event, Action.WARN, logReason);
                } else {
                    event.getChannel().sendMessage("This user does not exist. Please make sure to use either the ID or to mention (e.g. @Clyde#001).");
                }
            } catch (Exception e) {
                logger.warn("{} tried to warn a nonexistent user!", event.getMessageAuthor().getDiscriminatedName());
                event.getChannel().sendMessage("This user does not exist. Please make sure to use either the ID or to mention (e.g. @Clyde#001).");
            }

        } else {
            event.getChannel().sendMessage("You need to specify a user in order to warn him!");
        }
    }
}
