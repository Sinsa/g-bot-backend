package ch.discordbot.gbot.bot.commands;

import ch.discordbot.gbot.bot.Command;
import ch.discordbot.gbot.bot.utils.Usergroup;
import org.javacord.api.DiscordApi;
import org.javacord.api.event.message.MessageCreateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Help extends Command {
    static Logger logger = LoggerFactory.getLogger(Help.class);

    public Help(Usergroup minAccess, boolean serverOnly) {
        super(minAccess, serverOnly, "help");
    }

    public void executeCommand(MessageCreateEvent event, DiscordApi api, String[] params) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("How may I help you?\n\n**Available commands:**\n```")
                .append("g!ban  ")
                .append("\tBan a user. Must be at least ")
                .append(Usergroup.MODERATOR)
                .append(" to use this.\n")
                .append("g!check")
                .append("\tCheck a user's warns. Must be at least ")
                .append(Usergroup.MODERATOR)
                .append(" to use this.\n")
                .append("g!clear")
                .append("\tClears a whole text channel. Must be at least ")
                .append(Usergroup.MODERATOR)
                .append(" to use this.\n")
                .append("g!help ")
                .append("\tShows this help. Must be at least ")
                .append(Usergroup.USER)
                .append(" to use this.\n")
                .append("g!warn ")
                .append("\tWarn a user. Must be at least ")
                .append(Usergroup.MODERATOR)
                .append(" to use this.```");
        event.getChannel().sendMessage(stringBuilder.toString());
    }
}
