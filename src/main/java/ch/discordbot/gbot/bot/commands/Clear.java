package ch.discordbot.gbot.bot.commands;

import ch.discordbot.gbot.bot.Command;
import ch.discordbot.gbot.bot.utils.Usergroup;
import ch.discordbot.gbot.logging.Action;
import ch.discordbot.gbot.logging.ActionLogger;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.channel.ServerTextChannelUpdater;
import org.javacord.api.entity.server.Server;
import org.javacord.api.event.message.MessageCreateEvent;

public class Clear extends Command {
    ActionLogger actionLogger = new ActionLogger();

    public Clear(Usergroup minAccess, boolean serverOnly) {
        super(minAccess, serverOnly, "clear");
    }

    public void executeCommand(MessageCreateEvent event, DiscordApi api, String[] params) throws Exception {
        ServerTextChannel oldServerTextChannel = event.getServerTextChannel().get();
        Server server = event.getServer().get();
        ServerTextChannel newServerTextChannel = server.createTextChannelBuilder().setName(oldServerTextChannel.getName()).create().get();
        ServerTextChannelUpdater updater = newServerTextChannel.createUpdater();

        updater.setName(oldServerTextChannel.getName());
        updater.setTopic(oldServerTextChannel.getTopic());
        updater.setRawPosition(oldServerTextChannel.getRawPosition());
        updater.setSlowmodeDelayInSeconds(oldServerTextChannel.getSlowmodeDelayInSeconds());
        updater.setAuditLogReason(this.commandName + " was used by " + event.getMessageAuthor().getDiscriminatedName());
        updater.setNsfwFlag(oldServerTextChannel.isNsfw());
        if (oldServerTextChannel.getCategory().isPresent()) {
            updater.setCategory(oldServerTextChannel.getCategory().get());
        }

        updater.update();
        oldServerTextChannel.delete();
        actionLogger.log(event, Action.CLEAR, new String[0]);
    }
}
