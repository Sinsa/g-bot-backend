package ch.discordbot.gbot.bot;

import ch.discordbot.gbot.bot.commands.*;
import ch.discordbot.gbot.bot.utils.Usergroup;
import ch.discordbot.gbot.exceptions.MissingTokenException;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author Sinsa#2674
 * @version 0.1.0
 * @since 0.1.0
 */
@Component
public class Bot {

    static Logger messageLogger = LoggerFactory.getLogger("ch.discordbot.gbot.bot.Bot.MessageLogger");
    static Logger logger = LoggerFactory.getLogger(Bot.class);
    // Add all Commands here
    private final Clear clear = new Clear(Usergroup.MODERATOR, true);
    private final Check check = new Check(Usergroup.MODERATOR, true);
    private final Warn warn = new Warn(Usergroup.MODERATOR, true);
    private final Ban ban = new Ban(Usergroup.MODERATOR, true);
    private final Help help = new Help(Usergroup.USER, false);

    // Initialize the Bot here
    public void init(String[] args) {
        // Do basic bot Setup here
        if (args.length < 1) {
            throw new MissingTokenException("You need to provide a valid bot token in order for the bot to start! Use it as the first program argument.");
        }
        String token = args[0]; // Token must be the first Command Line Argument
        DiscordApi api = new DiscordApiBuilder().setToken(token).setAllIntents().login().join();
        logger.info("<init> I am ready");

        api.addMessageCreateListener(event -> {
            messageLogger.trace(event.getMessageAuthor().getDiscriminatedName() + ": " + event.getMessageContent());
            if (event.getMessageAuthor().isBotUser() || event.getChannel().getId() == 768037499699986452L) {
                logger.debug("Message from Bot recieved, ignoring");
                return;
            }
            if (!event.getMessageContent().startsWith("g!")) {
                // If the message does not start with the prefix it's none of our business
                return;
            }
            String[] params = event.getMessageContent().substring(2).split("\\s+");
            String baseCommand = params[0].toLowerCase();
            params = Arrays.stream(params).skip(1).toArray(String[]::new);
            try {
                switch (baseCommand) {
                    // Commands should be added here
                    case "hi":
                        logger.debug("hi command was triggered");
                        event.getChannel().sendMessage("Hello World!");
                        break;
                    case "clear":
                        logger.debug(clear.getCommandName() + " command was triggered");
                        if (hasUserPermission(clear, event) && isOnServerWithServerOnly(event, clear)) {
                            clear.executeCommand(event, api, params);
                        } else {
                            logAndSendNoPermissions(clear, logger, event);
                        }
                        break;
                    case "warn":
                        logger.debug(warn.getCommandName() + " command was triggered");
                        if (hasUserPermission(warn, event) && isOnServerWithServerOnly(event, warn)) {
                            warn.executeCommand(event, api, params);
                        } else {
                            logAndSendNoPermissions(warn, logger, event);
                        }
                        break;
                    case "ban":
                        logger.debug(ban.getCommandName() + " command was triggered");
                        if (hasUserPermission(ban, event) && isOnServerWithServerOnly(event, ban)) {
                            ban.executeCommand(event, api, params);
                        } else {
                            logAndSendNoPermissions(ban, logger, event);
                        }
                        break;
                    case "check":
                        logger.debug("{} command was triggered", check.getCommandName());
                        if (hasUserPermission(check, event) && isOnServerWithServerOnly(event, check)) {
                            check.executeCommand(event, api, params);
                        } else {
                            logAndSendNoPermissions(check, logger, event);
                        }
                        break;
                    case "help":
                        logger.debug("{} command was triggered", help.getCommandName());
                        help.executeCommand(event, api, params);
                        break;
                    case "rtfm":
                        event.getChannel().sendMessage("RTFM!");
                        break;
                }
            } catch (Exception e) {
                logger.error(e.toString());
                e.printStackTrace();
            }
        });
    }

    /**
     * Resolves the Usergroup a User belongs to.
     *
     * @param event The MessageCreateEvent that was fired
     * @return An Enum of Usergroup type, that belongs to the user
     */
    public Usergroup resolveCommandAuthorPermissionLevel(MessageCreateEvent event) {
        User authorAsUser;
        Server server;
        if (event.isServerMessage()) {
            authorAsUser = event.getMessageAuthor().asUser().get();
            server = event.getServer().get();
        } else {
            return Usergroup.USER;
        }
        if ((server.getPermissions(authorAsUser).getAllowedBitmask() & 0x8) == 0x8) {
            return (Usergroup.ADMINISTRATOR);
        } else if ((server.getPermissions(authorAsUser).getAllowedBitmask() & 0x2) == 0x2 && (server.getPermissions(authorAsUser).getAllowedBitmask() & 0x4) == 0x4) {
            return (Usergroup.MODERATOR);
        } else {
            return (Usergroup.USER);
        }
    }

    /**
     * Checks if a given user has enough permissions for a command.
     *
     * @param command The command the user wants to use
     * @param event   The MessageCreateEvent that was fired
     * @return If the user has permissions, true will be returned. False otherwise.
     */
    public boolean hasUserPermission(Command command, MessageCreateEvent event) {
        Usergroup minAccess = command.getMinAccess();
        Usergroup authorPermissions = resolveCommandAuthorPermissionLevel(event);
        return resolveUsergroupEnumValue(authorPermissions) >= resolveUsergroupEnumValue(minAccess);
    }

    /**
     * Sends Errormessages to the channel and to the console.
     *
     * @param command The Command that was used
     * @param logger  The Logger that should be used for the console error
     * @param event   The MessageCreateEvent that was fired
     */
    public void logAndSendNoPermissions(Command command, Logger logger, MessageCreateEvent event) {
        logger.warn("{} tried to use {} but has no Permissions! (Provided: {} | Required: {}", event.getMessageAuthor().getDiscriminatedName(), command.getCommandName(), resolveCommandAuthorPermissionLevel(event), command.getMinAccess());
        event.getChannel().sendMessage("Sorry <@" + event.getMessageAuthor().getIdAsString() + ">, aber du hast keine Berechtigungen für den Befehl!");
    }

    /**
     * This Method resolves a Usergroup to an integer so they are comparable.
     *
     * @param usergroup An Enum of Usergroup Type.
     * @return an Integer from 0-2.
     */
    public int resolveUsergroupEnumValue(Usergroup usergroup) {
        if (usergroup.equals(Usergroup.ADMINISTRATOR)) {
            return 2;
        } else if (usergroup.equals(Usergroup.MODERATOR)) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Checks if the serverOnly condition is met
     *
     * @param event   The MessageCreateEvent that is passed
     * @param command The command we want to check against
     * @return Wether the command is used in the right place or not
     */
    public boolean isOnServerWithServerOnly(MessageCreateEvent event, Command command) {
        if (!command.isServerOnly()) {
            return false;
        } else {
            return event.getServerTextChannel().isPresent();
        }
    }
}
