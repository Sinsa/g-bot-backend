package ch.discordbot.gbot.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MissingTokenException extends RuntimeException {
    Logger logger = LoggerFactory.getLogger(MissingTokenException.class);

    public MissingTokenException(String reason) {
        super(reason);
        logger.error("Missing Token Exception was thrown! Reason: " + reason);
        System.exit(1);
    }
}
