package ch.discordbot.gbot.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatabaseException extends RuntimeException {
    Logger logger = LoggerFactory.getLogger(DatabaseException.class);

    public DatabaseException(String reason) {
        super(reason);
        logger.warn("Database Exception was thrown! Reason: " + reason);
    }
}
