package ch.discordbot.gbot;

import ch.discordbot.gbot.bot.Bot;
import org.apache.maven.Maven;
import org.apache.maven.cli.MavenCli;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.IOException;

@SpringBootApplication
public class GBotApplication {
    static Logger logger = LoggerFactory.getLogger(GBotApplication.class);

    public static void main(String[] args) {
        //we migrate all Flyway Migrations first
        try {
            MavenCli maven = new MavenCli();
            maven.doMain(new String[]{"flyway:migrate"}, new File(".").toString(), System.out, System.out);
        } catch (RuntimeException exception) {
            logger.error("Maven execution failed! Reason: {}", exception.getMessage());
            logger.error("Exiting because of an earlier maven failure.");
            System.exit(1);
        }

        //then we start the API and the backend
        SpringApplication.run(GBotApplication.class, args);

        //finally we initialize the bot
        Bot bot = new Bot();
        bot.init(args);
    }

}
