package ch.discordbot.gbot.rest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigController {

    @GetMapping("/")
    public String getRoot() {
        return "Hello World!";
    }

    @GetMapping("/config")
    public String getConfigRoot() {
        return "Root of directory \"Config\".";
    }
}
