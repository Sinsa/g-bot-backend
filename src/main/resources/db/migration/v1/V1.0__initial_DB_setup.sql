CREATE TABLE warn (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_id VARCHAR(20) NOT NULL,
    staff_id VARCHAR(20) NOT NULL,
    reason varchar(200),
    guild_id VARCHAR(20) NOT NULL
);
